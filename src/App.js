import React, { Component } from 'react';
import Task from './component/task'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: []
    };
  };
  add = (text) => {
      var arr = this.state.tasks;
      arr.push (text);
      this.setState ({tasks: arr});
  };
  deleteBlock = (i) => {
    var arr = this.state.tasks;
    arr.splice (i, 1);
    this.setState ({tasks: arr});
  };
  updateText = (text, i) => {
    var arr = this.state.tasks;
    arr[i] = text;
    this.setState ({tasks: arr});
  };
  eachTask = (item, i) => {
    return (
      <Task key={i} index={i} update={this.updateText} deleteBlock={this.deleteBlock}>
        {item}
      </Task>
    );
  };
  render() {
    return (
      <div className="field">
        <button onClick={this.add.bind (null, 'Простое задание')} className="btn new">Новое задание</button>
        {this.state.tasks.map (this.eachTask)}
      </div>
    );
  }
}

export default App;
